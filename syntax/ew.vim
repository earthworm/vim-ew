" Vim syntax file
" Language:     Earthworm configuration files
" Maintainer:   Matteo Quintiliani <quintiliani@ingv.it>
" Last Change:  2008 May
" 8-colors 

" For version 5.x: Clear all syntax items
" For version 6.x: Quit when a syntax file was already loaded
if version < 600
  syntax clear
elseif exists("b:current_syntax")
  finish
endif


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Syntax for file .desc
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
syn match	desctsecLine		"^tsec:.*"
syn match	descerrLine		"^err:.*"
syn match	desctextLine		"^text:.*"
syn keyword	descKeyword		modName modId instId restartMe

syn region	descArgumentParam	display start='<' end='>' contains=descKeyword
syn match	descComment		"#.*$"	contains=descKeyword,descArgumentParam

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Syntax for file .d
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
syn keyword	dKeyword		contained MyModuleId HeartBeatInterval LogFile

syn match	dInst			"INST_[^ ]*"
syn match	dModule			"MOD_[^ ]*"
syn match	dRing			"[^ ]*_RING[^ ]*"
syn match	dType			"TYPE_[^ ]*"

syn match	dTest			display "^[ ]*[^\#]" nextgroup=dTestA
syn match	dTestA			contained "[^ \t\#]\+" contains=dKeyword nextgroup=dTestB
syn match	dTestB			contained "[ \t]*[^\#]*" contains=dInst,dModule,dRing,dType

syn match	dinclude		"^[ ]*@[^ \t]*"


if version >= 508 || !exists("did_ew_syntax_inits")
  if version < 508
    let did_ew_syntax_inits = 1
    command -nargs=+ HiLink hi link <args>
  else
    command -nargs=+ HiLink hi def link <args>
  endif

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
hi descKeyword		ctermfg=1
hi dKeyword		ctermfg=1

hi dInst		ctermfg=3
hi dModule		ctermfg=3
hi dRing		ctermfg=3
hi dType		ctermfg=3

hi dTest		ctermfg=1
hi dTestA		ctermfg=1
hi dTestB		ctermfg=2

hi dinclude		cterm=underline		ctermfg=6

hi desctsecLine		ctermfg=1
hi descerrLine		ctermfg=1
hi desctextLine		ctermfg=1
hi descArgumentParam	ctermfg=2
hi descComment		ctermfg=4

  
  delcommand HiLink
endif

let b:current_syntax = "ew"

