" Vim syntax file
" Language:     Hipoinverse Y2000 Output Format
" Maintainer:   Matteo Quintiliani <quintiliani@ingv.it>
" Last Change:  2008 May
" 8-colors 

" For version 5.x: Clear all syntax items
" For version 6.x: Quit when a syntax file was already loaded
if version < 600
  syntax clear
elseif exists("b:current_syntax")
  finish
endif


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Y2000 summary format (also used as header in archive file)
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" We suppose that the line begins with [0-9],
" that is the first number of the Year
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" 1 4 I4 Year. *
" 5 8 4I2 Month, day, hour and minute.
syn match SumDateYYYYMMDD		"^[0-9]\{8}"
syn match SumHourMinute			"^[0-9].\{7}[0-9]\{4}"lc=8
" 13 4 F4.2 Origin time seconds.
syn match SumOriginTime			"^[0-9].\{11}[- 0-9]\{4}"lc=12
" 17 2 F2.0 Latitude (deg). First character must not be blank.
syn match SumLatitude			"^[0-9].\{15}[ 0-9]\{2}"lc=16
" 19 1 A1 S for south, blank otherwise.
syn match SumSouth			"^[0-9].\{17}[ S]\{1}"lc=18
" 20 4 F4.2 Latitude (min).
syn match SumLatitudeMin		"^[0-9].\{18}[ 0-9]\{4}"lc=19
" 24 3 F3.0 Longitude (deg).
syn match SumLongitude			"^[0-9].\{22}[ 0-9]\{3}"lc=23
" 27 1 A1 E for east, blank otherwise.
syn match SumEast			"^[0-9].\{25}[ E]\{1}"lc=26
" 28 4 F4.2 Longitude (min).
syn match SumLongitudeMin		"^[0-9].\{26}[ 0-9]\{4}"lc=27
" 32 5 F5.2 Depth (km).
syn match SumDepth			"^[0-9].\{30}[- 0-9]\{5}"lc=31
" 37 3 F3.2 Amplitude magnitude. *
syn match SumAmplitudeMag		"^[0-9].\{35}[- 0-9]\{3}"lc=36
" 40 3 I3 Number of P & S times with final weights greater than 0.1.
syn match SumNumberPS			"^[0-9].\{38}[- 0-9]\{3}"lc=39
" 43 3 I3 Maximum azimuthal gap, degrees.
syn match SumMaximumAzimuthalGap	"^[0-9].\{41}[- 0-9]\{3}"lc=42
" 46 3 F3.0 Distance to nearest station (km).
syn match SumDistanceNearestSta		"^[0-9].\{44}[- 0-9]\{3}"lc=45
" 49 4 F4.2 RMS travel time residual.
syn match SumRMS			"^[0-9].\{47}[- 0-9]\{4}"lc=48
" 53 3 F3.0 Azimuth of largest principal error (deg E of N).
syn match SumAzimuthLargest		"^[0-9].\{51}[- 0-9]\{3}"lc=52
" 56 2 F2.0 Dip of largest principal error (deg).
syn match SumDipLargest			"^[0-9].\{54}[- 0-9]\{2}"lc=55
" 58 4 F4.2 Size of largest principal error (km).
syn match SumSizeLargest		"^[0-9].\{56}[- 0-9]\{4}"lc=57
" 62 3 F3.0 Azimuth of intermediate principal error.
syn match SumAzimuthIntermediate	"^[0-9].\{60}[- 0-9]\{3}"lc=61
" 65 2 F2.0 Dip of intermediate principal error.
syn match SumDipIntermediate		"^[0-9].\{63}[- 0-9]\{2}"lc=64
" 67 4 F4.2 Size of intermediate principal error (km).
syn match SumSizeIntermediate		"^[0-9].\{65}[- 0-9]\{4}"lc=66
" 71 3 F3.2 Coda duration magnitude. *
syn match SumCodaDurMag			"^[0-9].\{69}[- 0-9]\{3}"lc=70
" 74 3 A3 Event location remark (region), derived from location.
syn match SumEventLocationRemark	"^[0-9].\{72}[ A-Z0-9]\{3}"lc=73
" 77 4 F4.2 Size of smallest principal error (km).
syn match SumSizeSmallest		"^[0-9].\{75}[- 0-9]\{4}"lc=76
" 81 1 A1 Auxiliary remark from analyst (i.e. Q for quarry).
syn match SumAux1			"^[0-9].\{79}[ #A-Z0-9]\{1}"lc=80
" 82 1 A1 Auxiliary remark from program (i.e. �-� for depth fixed, etc.).
syn match SumAux2			"^[0-9].\{80}[ #A-Z0-9]\{1}"lc=81
" 83 3 I3 Number of S times with weights greater than 0.1.
syn match SumNumberS			"^[0-9].\{81}[- 0-9]\{3}"lc=82
" 86 4 F4.2 Horizontal error (km).
syn match SumHorizontalErr		"^[0-9].\{84}[- 0-9]\{4}"lc=85
" 90 4 F4.2 Vertical error (km).
syn match SumVerticalErr		"^[0-9].\{88}[- 0-9]\{4}"lc=89
" 94 3 I3 Number of P first motions. *
syn match SumNumberP			"^[0-9].\{92}[- 0-9]\{3}"lc=93
" 97 4 F4.1 Total of amplitude mag weights ~number of readings.*
syn match SumTotAmplMagWeigths		"^[0-9].\{95}[- 0-9]\{4}"lc=96
" 101 4 F4.1 Total of duration mag weights ~number of readings. *
syn match SumTotDurMagWeigths		"^[0-9].\{99}[- 0-9]\{4}"lc=100
" 105 3 F3.2 Median-absolute-difference of amplitude magnitudes.
syn match SumMedAbsDiffAmpMag		"^[0-9].\{103}[- 0-9]\{3}"lc=104
" 108 3 F3.2 Median-absolute-difference of duration magnitudes.
syn match SumMedAbsDiffDurMag		"^[0-9].\{106}[- 0-9]\{3}"lc=107
" 111 3 A3 3-letter code of crust and delay model.
syn match SumCodeCrustModel		"^[0-9].\{109}[ A-Za-z0-9]\{3}"lc=110
" 114 1 A1 �Authority� code, i.e. what network furnished the information.
"          Hypoinverse passes this code through.
syn match SumAuthorityCode		"^[0-9].\{112}[ A-Za-z0-9]\{1}"lc=113
" 115 1 A1 Most common P & S data source code. (See table 1 below).
syn match SumMostCommonPS		"^[0-9].\{113}[ A-Za-z0-9]\{1}"lc=114
" 116 1 A1 Most common duration data source code. (See cols. 68-69)
syn match SumMostCommonDur		"^[0-9].\{114}[ A-Za-z0-9]\{1}"lc=115
" 117 1 A1 Most common amplitude data source code.
syn match SumMostCommonAmpl		"^[0-9].\{115}[ A-Za-z0-9]\{1}"lc=116
" 118 1 A1 Primary coda duration magnitude type code
syn match SumPrimaryCoda		"^[0-9].\{116}[ A-Za-z0-9]\{1}"lc=117
" 119 3 I3 Number of valid P & S readings (assigned weight > 0).
syn match SumNumberValidPS		"^[0-9].\{117}[- 0-9]\{3}"lc=118
" 122 1 A1 Primary amplitude magnitude type code
syn match SumPrimaryAmpl		"^[0-9].\{120}[ A-Za-z0-9]\{1}"lc=121
" 123 1 A1 'External' magnitude label or type code. Typically 'L' (=ML) This
"          information is not computed by Hypoinverse, but passed along, as computed by UCB.
syn match SumExtMagLabel		"^[0-9].\{121}[ A-Za-z0-9]\{1}"lc=122
" 124 3 F3.2 'External' magnitude.
syn match SumExtMag			"^[0-9].\{122}[- 0-9]\{3}"lc=123
" 127 3 F3.1 Total of 'external' magnitude weights (~ number of readings).
syn match SumTotExtMag			"^[0-9].\{125}[- 0-9]\{3}"lc=126
" 130 1 A1 Alternate amplitude magnitude label or type code.
syn match SumAltAmplMagLabel		"^[0-9].\{128}[ A-Za-z0-9]\{1}"lc=129
" 131 3 F3.2 Alternate amplitude magnitude.
syn match SumAltAmplMag			"^[0-9].\{129}[- 0-9]\{3}"lc=130
" 134 3 F3.1 Total of the alternate amplitude mag weights ~no. of readings.
syn match SumTotAltMag			"^[0-9].\{132}[- 0-9]\{3}"lc=133
" 137 10 I10 Event identification number
syn match SumEventID			"^[0-9].\{135}[- 0-9]\{10}"lc=136
" 147 1 A1 Preferred magnitude label code chosen from those available.
syn match SumPreferMagCode		"^[0-9].\{145}[ A-Za-z0-9]\{1}"lc=146
" 148 3 F3.2 Preferred magnitude, chosen by the Hypoinverse PRE command.
syn match SumPreferMag			"^[0-9].\{146}[- 0-9]\{3}"lc=147
" 151 4 F4.1 Total of the preferred mag weights (~ number of readings). *
syn match SumTotPreferMag		"^[0-9].\{149}[- 0-9]\{4}"lc=150
" 155 1 A1 Alternate coda duration magnitude label or type code.
syn match SumAltCodaDurLabel		"^[0-9].\{153}[ A-Za-z0-9]\{1}"lc=154
" 156 3 F3.2 Alternate coda duration magnitude.
syn match SumAltCodaDurMag		"^[0-9].\{154}[- 0-9]\{3}"lc=155
" 159 4 F4.1 Total of the alternate coda duration magnitude weights. *
syn match SumTotAltCodaDurMag		"^[0-9].\{157}[- 0-9]\{4}"lc=158
" 163 1 A1 �Version� of the information, i.e. the stage of processing. This
"          can either be passed through, or assigned by Hypoinverse with the LAB command.
syn match SumVersionInfo		"^[0-9].\{161}[ A-Za-z0-9]\{1}"lc=162
" 164 1 A1 Version of last human review. Hypoinverse passes this through.
syn match SumVersionInfoHuman		"^[0-9].\{162}[ A-Za-z0-9]\{1}"lc=163
" 164 is the last filled column. 
 

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Y2000 (station) archive format
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" We suppose that the line begins with [A-Z],
" that is the first character of Station Code
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" 1 5 A5 5-letter station site code, left justified. *
syn match Station		"^[A-Z][A-Z0-9 ]\{4}"
" 6 2 A2 2-letter seismic network code. *
syn match Network		"^[A-Z].\{4}[A-Z0-9 ]\{2}"lc=5
" 8 1 1X Blank *
" 9 1 A1 One letter station component code.
syn match Component1		"^[A-Z].\{7}[A-Z]\{1}"lc=8
" 10 3 A3 3-letter station component code. *
syn match ComponentCode		"^[A-Z].\{8}[A-Z]\{3}"lc=9
" 13 1 1X Blank *
" 14 2 A2 P remark such as 'IP'.
syn match Premark		"^[A-Z].\{12}[A-Za-z ]\{2}"lc=13
" 16 1 A1 P first motion.
syn match PFirstMotion		"^[A-Z].\{14}[A-Z ]\{1}"lc=15
" 17 1 I1 Assigned P weight code.
syn match PWeightCode		"^[A-Z].\{15}[0-9 ]\{1}"lc=16
" 18 4 I4 Year. *
syn match DateYYYYMMDD		"^[A-Z].\{16}[0-9]\{8}"lc=17
" 22 8 4I2 Month, day, hour and minute.
syn match TimeHHMM		"^[A-Z].\{24}[0-9]\{4}"lc=25
" 30 5 F5.2 Second of P arrival.
syn match SecondP		"^[A-Z].\{28}[ 0-9]\{5}"lc=29
" 35 4 F4.2 P travel time residual.
syn match PTravTimeRes		"^[A-Z].\{33}[- 0-9]\{4}"lc=34
" 39 3 F3.2 P weight actually used.
syn match PWeight		"^[A-Z].\{37}[ 0-9]\{3}"lc=38
" 42 5 F5.2 Second of S arrival.
syn match SecondS		"^[A-Z].\{40}[ 0-9]\{5}"lc=41
" 47 2 A2 S remark such as 'ES'.
" 49 1 1X Blank
syn match Sremark		"^[A-Z].\{45}[ A-Za-z]\{2}"lc=46
" 50 1 I1 Assigned S weight code.
syn match SWeightCode		"^[A-Z].\{48}[0-9 ]\{1}"lc=49
" 51 4 F4.2 S travel time residual.
syn match STravTimeRes		"^[A-Z].\{49}[- 0-9]\{4}"lc=50
" 55 7 F7.2 Amplitude (Normally peak-to-peak). *
syn match Amplitude		"^[A-Z].\{53}[- 0-9]\{7}"lc=54
" 62 2 I2 Amp units code. 0=PP mm, 1=0 to peak mm (UCB), 2=digital counts. *
syn match AmplitudeUnit		"^[A-Z].\{60}[- 0-9]\{2}"lc=61
" 64 3 F3.2 S weight actually used.
syn match SWeight		"^[A-Z].\{62}[ 0-9]\{3}"lc=63
" 67 4 F4.2 P delay time.
syn match PDelay		"^[A-Z].\{65}[ 0-9]\{4}"lc=66
" 71 4 F4.2 S delay time.
syn match SDelay		"^[A-Z].\{69}[ 0-9]\{4}"lc=70
" 75 4 F4.1 Epicentral distance (km).
syn match EpicentralDist	"^[A-Z].\{73}[ 0-9]\{4}"lc=74
" 79 3 F3.0 Emergence angle at source.
syn match EmergenceAngle	"^[A-Z].\{77}[ 0-9]\{3}"lc=78
" 82 1 I1 Amplitude magnitude weight code.
syn match AmplitudeWeightCode	"^[A-Z].\{80}[0-9 ]\{1}"lc=81
" 83 1 I1 Duration magnitude weight code.
syn match DurationWeightCode	"^[A-Z].\{81}[0-9 ]\{1}"lc=82
" 84 3 F3.2 Period at which the amplitude was measured for this station.
syn match PeriodAmplitude	"^[A-Z].\{82}[ 0-9]\{3}"lc=83
" 87 1 A1 1-letter station remark.
syn match LetterStationRemark	"^[A-Z].\{85}[A-Z]\{1}"lc=86
" 88 4 F4.0 Coda duration in seconds.
syn match CodaDuration		"^[A-Z].\{86}[ 0-9]\{4}"lc=87
" 92 3 F3.0 Azimuth to station in degrees E of N.
syn match Azimuth		"^[A-Z].\{90}[ 0-9]\{3}"lc=91
" 95 3 F3.2 Duration magnitude for this station. *
syn match DurationMagnitude	"^[A-Z].\{93}[ 0-9]\{3}"lc=94
" 98 3 F3.2 Amplitude magnitude for this station. *
syn match AmplitudeMagnitude	"^[A-Z].\{96}[ 0-9]\{3}"lc=97
" 101 4 F4.3 Importance of P arrival.
syn match ImportancePArrival	"^[A-Z].\{99}[ 0-9]\{4}"lc=100
" 105 4 F4.3 Importance of S arrival.
syn match ImportanceSArrival	"^[A-Z].\{103}[ 0-9]\{4}"lc=104
" 109 1 A1 Data source code.
syn match DataSourceCode	"^[A-Z].\{107}[ A-Z]\{1}"lc=108
" 110 1 A1 Label code for duration magnitude from FC1 or FC2 command.
syn match LabelDurationMag	"^[A-Z].\{108}[ A-Z]\{1}"lc=109
" 111 1 A1 Label code for amplitude magnitude from XC1 or XC2 command.
syn match LabelAmplitudeMag	"^[A-Z].\{109}[ A-Z]\{1}"lc=110
" 112 2 A2 2-letter station location code (component extension).
syn match Location		"^[A-Z].\{110}[ -A-Z0-9]\{2}"lc=111
" 113 is the last filled column


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Lines that contain Event-ID
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
syn match LineID	"^[ ]\{62}[ 0-9]\{10}"


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Lines that begin with $
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
syn match DollarLine "^\$.*$"


if version >= 508 || !exists("did_arc_syntax_inits")
  if version < 508
    let did_arc_syntax_inits = 1
    command -nargs=+ HiLink hi link <args>
  else
    command -nargs=+ HiLink hi def link <args>
  endif

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
hi SumDateYYYYMMDD		cterm=underline		ctermfg=1
hi SumHourMinute		cterm=underline		ctermfg=2
hi SumOriginTime		cterm=underline		ctermfg=4
hi SumLatitude			ctermfg=0		ctermbg=7
hi SumSouth			ctermfg=5		ctermbg=7
hi SumLatitudeMin		ctermfg=0		ctermbg=7
hi SumLongitude			ctermfg=0		ctermbg=3
hi SumEast			ctermfg=5		ctermbg=3
hi SumLongitudeMin		ctermfg=0		ctermbg=3
hi SumDepth			ctermfg=7		ctermbg=4
hi SumAmplitudeMag		ctermfg=2
hi SumNumberPS			ctermfg=5
hi SumMaximumAzimuthalGap	cterm=underline		ctermfg=6
hi SumDistanceNearestSta	ctermfg=7		ctermbg=0
hi SumRMS			ctermfg=2
hi SumAzimuthLargest		ctermfg=1
hi SumDipLargest		ctermfg=2
hi SumSizeLargest		ctermfg=6
hi SumAzimuthIntermediate	ctermfg=4
hi SumDipIntermediate		ctermfg=5
hi SumSizeIntermediate		ctermfg=6
hi SumCodaDurMag		ctermfg=5
hi SumEventLocationRemark	ctermfg=2
hi SumSizeSmallest		ctermfg=1
hi SumAux1			ctermfg=2
hi SumAux2			ctermfg=6
hi SumNumberS			ctermfg=4
hi SumHorizontalErr		ctermfg=5
hi SumVerticalErr		ctermfg=6
hi SumNumberP			ctermfg=5
hi SumTotAmplMagWeigths		ctermfg=2
hi SumTotDurMagWeigths		ctermfg=1
hi SumMedAbsDiffAmpMag		ctermfg=2
hi SumMedAbsDiffDurMag		ctermfg=6
hi SumCodeCrustModel		ctermfg=4
hi SumAuthorityCode		ctermfg=5
hi SumMostCommonPS		ctermfg=6
hi SumMostCommonDur		ctermfg=5
hi SumMostCommonAmpl		ctermfg=2
hi SumPrimaryCoda		ctermfg=1
hi SumNumberValidPS		ctermfg=2
hi SumPrimaryAmpl		ctermfg=6
hi SumExtMagLabel		ctermfg=4
hi SumExtMag			ctermfg=5
hi SumTotExtMag			ctermfg=6
hi SumAltAmplMagLabel		ctermfg=5
hi SumAltAmplMag		ctermfg=2
hi SumTotAltMag			ctermfg=1
hi SumEventID			ctermfg=2
hi SumPreferMagCode		ctermfg=6
hi SumPreferMag			ctermfg=4
hi SumTotPreferMag		ctermfg=5
hi SumAltCodaDurLabel		ctermfg=6
hi SumAltCodaDurMag		ctermfg=5
hi SumTotAltCodaDurMag		ctermfg=2
hi SumVersionInfo		ctermfg=1
hi SumVersionInfoHuman		ctermfg=2
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
hi Station			ctermfg=2
hi Network			ctermfg=1
hi Component1			ctermfg=2
hi ComponentCode		ctermfg=4
hi Premark			ctermfg=1
hi PFirstMotion			ctermfg=5
hi PWeightCode			ctermfg=6
hi DateYYYYMMDD			cterm=underline		ctermfg=1
hi TimeHHMM			cterm=underline		ctermfg=2
hi SecondP			cterm=underline		ctermfg=4
hi PTravTimeRes			ctermfg=2
hi PWeight			ctermfg=5
hi SecondS			cterm=underline		ctermfg=4
hi Sremark			ctermfg=1
hi SWeightCode			ctermfg=6
hi STravTimeRes			ctermfg=5
hi Amplitude			ctermfg=2
hi AmplitudeUnit		ctermfg=1
hi SWeight			ctermfg=2
hi PDelay			ctermfg=6
hi SDelay			ctermfg=4
hi EpicentralDist		ctermfg=5
hi EmergenceAngle		ctermfg=6
hi AmplitudeWeightCode		ctermfg=5
hi DurationWeightCode		ctermfg=2
hi PeriodAmplitude		ctermfg=1
hi LetterStationRemark		ctermfg=2
hi CodaDuration			ctermfg=6
hi Azimuth			ctermfg=4
hi DurationMagnitude		ctermfg=5
hi AmplitudeMagnitude		ctermfg=6
hi ImportancePArrival		ctermfg=5
hi ImportanceSArrival		ctermfg=2
hi DataSourceCode		ctermfg=1
hi LabelDurationMag		ctermfg=2
hi LabelAmplitudeMag		ctermfg=5
hi Location			ctermfg=4
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
hi LineID			ctermfg=5
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
hi DollarLine			ctermfg=3
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
  
  delcommand HiLink
endif

let b:current_syntax = "arc"

