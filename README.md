# Earthworm VIM syntax highlighting
 * Hypoinverse ARC files
 * Earthworm .d and .desc files

### Installation

Require `pathogen.vim`

https://github.com/tpope/vim-pathogen

```
mkdir -p ~/.vim/autoload ~/.vim/bundle && \
curl -LSso ~/.vim/autoload/pathogen.vim https://tpo.pe/pathogen.vim
```

Add to `~/.vimrc` the following line

```
execute pathogen#infect()
```

```
cd ~/.vim/bundle
git clone git@gitlab.rm.ingv.it:earthworm/vim-ew.git
# OR
# git clone https://gitlab.rm.ingv.it/earthworm/vim-ew.git
```
